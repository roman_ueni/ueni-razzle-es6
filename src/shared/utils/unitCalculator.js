const splitUnit = unit => {
  const [, value, type] = unit.match(/(\d*\.?\d*)(.*)/)
  return [value, type]
}

const calculate = calculation => (unit, number) => {
  const [value, type] = splitUnit(unit)
  return `${calculation(Number(value), Number(number))}${type}`
}

const unitCalculator = {
  add: calculate((value, number) => value + number),
  minus: calculate((value, number) => value - number),
  multiply: calculate((value, number) => value * number),
  divide: calculate((value, number) => value / number),
}

export const convertUnitToNumber = unit => splitUnit(unit)[0]

export default unitCalculator
