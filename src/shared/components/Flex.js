import React from 'react'
import { pick, without } from 'lodash'
import styled from 'styled-components'
// eslint-disable-next-line no-restricted-imports
import { Box, Flex as FlexOriginal } from 'grid-styled'

const deprecatedPropTypes = ['align', 'wrap', 'justify']
const flexProps = without(
  [...Object.keys(Box.propTypes), ...Object.keys(FlexOriginal.propTypes), 'className', 'onClick', 'data-id'],
  ...deprecatedPropTypes
)

export const Flex = styled(({ children, ...props }) => {
  const passToFlex = pick(props, flexProps)
  return <FlexOriginal { ...passToFlex }>{children}</FlexOriginal>
})``

export const CenteredFlex = Flex.extend.attrs({ justifyContent: 'center', alignItems: 'center' })``
