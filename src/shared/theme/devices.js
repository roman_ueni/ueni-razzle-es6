import { css } from 'styled-components'

const breakpoints = {
  desktop: 1171,
  mobile: 450,
  smallTablet: 580,
  tablet: 743,
  largeTablet: 768,
}

export default Object.keys(breakpoints).reduce((acc, label) => {
  acc[label] = (...args) =>
    css`
      @media (max-width: ${breakpoints[label]}px) {
        ${css(...args)};
      }
    `
  return acc
}, {})
