// @flow
import { css } from 'styled-components'
import { transform } from 'lodash'

export const defaultMediaBreakpoints = {
  phoneLandscape: '36rem', // 576px
  tablet: '48rem', // 768px
  desktop: '62rem', // 992px
  largeDesktop: '75rem', // 1200px
}

// TODO: move to factories and refactor to work with mediaBreakpointsOverride and different themes then the default
export default transform(defaultMediaBreakpoints, (result, breakPoint, breakPointName) => {
  result[breakPointName] = (...args) => // eslint-disable-line no-param-reassign
    css`
      @media (min-width: ${breakPoint}) {
        ${css(...args)};
      }
    `
})
