import { values, times } from 'lodash'
import { convertUnitToNumber } from '@shared/utils/unitCalculator'

import { defaultMediaBreakpoints } from './media'

import * as fonts from './fonts'
import * as textStyles from './textStyles'
import * as iconSizes from './iconSizes'

export { fonts, textStyles, iconSizes }

export { default as colors } from './colors'

export const grid = {
  columnOffset: '1rem', // 16px
  columnGutter: '2rem', // 32px
  gutter: '4rem', // 64px
  columns: 12,
}

export const maxPageWidth = defaultMediaBreakpoints.largeDesktop

export const breakpoints = values(defaultMediaBreakpoints).sort(
  (a, b) => convertUnitToNumber(a) - convertUnitToNumber(b)
)

export const space = [0, ...times(10, index => `${index + 1}rem`)]
