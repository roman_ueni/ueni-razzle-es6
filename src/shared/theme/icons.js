const fontelloConfig = require('./fontello-config.json')

const initialIconsCss = `
  .uenicon::before{
    display: inline-block;
    font-family: UENIcons;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    text-rendering: auto;
    text-transform: none;
  }
`

export default fontelloConfig.glyphs.reduce(
  (iconsCss, iconConfig) => `
    ${iconsCss}
    .uenicon-${iconConfig.css}::before{
      content: "\\${iconConfig.code.toString(16)}";
    }
  `,
  initialIconsCss
)
