// eslint-disable-next-line no-restricted-imports
import { injectGlobal } from 'styled-components'
import calendarStylesheet from 'react-day-picker/lib/style.css'
import imageGalleryTheme from 'react-image-gallery/styles/css/image-gallery.css'

import { regularFontFamily } from '@shared/theme/fonts'

import icons from './icons'

// eslint-disable-next-line no-unused-expressions
injectGlobal`
  @font-face {
    font-family: UENIcons;
  }
  @font-face {
    font-family: 'Lato';
    src: url('/static/fonts/Lato-Regular.ttf');
  }
  @font-face {
    font-family: 'Lato';
    src: url('/static/fonts/Lato-Light.ttf');
    font-weight: 300;
  }
  @font-face {
    font-family: 'Lato';
    src: url('/static/fonts/Lato-Bold.ttf');
    font-weight: 700;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Regular.ttf');
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Regular.ttf');
    font-weight: normal;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Medium.ttf');
    font-weight: 500;
  }

  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Black.ttf');
    font-weight: 600;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Black.ttf');
    font-weight: 800;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Black.ttf');
    font-weight: bold;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Italic.ttf');
    font-weight: 300;
    font-style: italic;
  }
  @font-face {
    font-family: 'MontserratUENI';
    src: url('/static/fonts/Montserrat-Italic.ttf');
    font-weight: 400;
    font-style: italic;
  }

  html, body {
    margin: 0;
    ${regularFontFamily}
    line-height: 1.5rem;
    height: 100%;
  }

  * {
    box-sizing: border-box;
    position: relative;
  }

  *::-ms-clear {
    display: none;
  }

  button {
    background: none;
    border: none;
    cursor: pointer;
    outline: none;
  }

  h1, h2, h3, h4, h5, p {
    margin-bottom: 0;
    margin-top: 0;
  }

  a {
    color: inherit;
    cursor: pointer;
    text-decoration: none;
  }

  ${icons}

  ${calendarStylesheet}
  ${imageGalleryTheme}

  ::-webkit-scrollbar {
    height: 7px;
    width: 7px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #999999;
  }

  ::-webkit-scrollbar-track {
    background-clip: content-box;
    background-color: #C0C0C0;
    border-color: transparent;
    border-style: solid;
    border-width: 3px;
  }

  input::placeholder {
    overflow: hidden;
    text-overflow:ellipsis !important;
    white-space: nowrap;
  }

  input[type="number"] {
    appearance: none;
  }

  :invalid, :-moz-submit-invalid, :-moz-ui-invalid {
    box-shadow: none;
  }
`
