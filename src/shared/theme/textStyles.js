import { css } from 'styled-components'
import devices from './devices'

import { headingFont, regularFont } from './fonts'

const heading = css`
  font-family: ${headingFont};
  font-weight: 500;
`

const textFont = css`
  font-family: ${regularFont};
`

export const h1 = css`
  ${heading}
  font-size: 46px;
  line-height: 54px;
  ${devices.tablet`
    font-size: 32px;
    line-height: 40px;
  `};
`

export const h2 = css`
  ${heading}
  font-size: 32px;
  line-height: 40px;
  ${devices.tablet`
    font-size: 24px;
    line-height: 32px;
  `};
`

export const h3 = css`
  ${heading}
  font-size: 24px;
  line-height: 32px;
  ${devices.tablet`
    font-size: 20px;
    line-height: 28px;
  `};
`

export const h4 = css`
  ${heading}
  font-weight: 500;
  font-size: 18px;
  line-height: 26px;
  ${devices.tablet`
    font-size: 16px;
    line-height: 24px;
  `};
`

export const regular = css`
  ${textFont}
  font-size: 18px;
  font-weight: 300;
  line-height: 26px;
  ${devices.tablet`
    font-size: 16px;
    line-height: 24px;
  `};
`

export const medium = css`
  ${textFont}
  font-size: 14px;
  font-weight: 400;
  line-height: 22px;
  ${devices.tablet`
    font-size: 12px;
    line-height: 20px;
  `};
`

export const small = css`
  ${textFont}
  font-size: 12px;
  font-weight: 400;
  line-height: 20px;
`
