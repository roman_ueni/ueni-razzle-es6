import { isNil } from 'lodash'
import { css } from 'styled-components'
import * as defaultTheme from './theme'

/* eslint no-bitwise: [2, { allow: [">>", "&"] }] */
const hexToRgbA = (hex, opacity) => {
  let c
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('')
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = `0x${c.join('')}`
    return `rgba(${(c >> 16) & 255}, ${(c >> 8) & 255}, ${c & 255}, ${opacity > 1 ? opacity / 100 : opacity})`
  }
  return null
}

export const getTextStyle = (textStyleOverride, themeOverride) => ({ textStyle, theme }) => {
  const { textStyles } = themeOverride || theme || defaultTheme
  return textStyles[textStyleOverride || textStyle] || textStyles.textFont
}

export const getIconSize = (iconSizeOverride, themeOverride) => ({ iconSize, theme }) => {
  const { iconSizes } = themeOverride || theme || defaultTheme
  return iconSizes[iconSizeOverride || iconSize] || iconSizes.medium
}

export const getColor = (colorOverride, themeOverride) => ({ color, theme }) => {
  console.log('GET', color, theme)
  const { colors } = themeOverride || theme || defaultTheme
  return colors[colorOverride || color] || colorOverride || color
}

export const getColorWithDefaultTheme = color => getColor(color)({ defaultTheme })

export const getSizeProps = ({ height, width }) => css`
  ${!isNil(height) && `height: ${height};`} ${!isNil(width) && `width: ${width};`};
`

export const getTextProps = ({ color, bold, font, light, underline }) => css`
  color: ${getColor(color || 'primary')};
  ${getTextStyle(font)}
  ${bold && 'font-weight: bold;'}
  ${light && 'font-weight: lighter;'}
  ${underline && 'text-decoration: underline;'}
`

export const getBackground = (color, opacity = 1, theme = defaultTheme) => css`
  background: ${getColor(color || 'primary')};
  background: ${hexToRgbA(getColor(color || 'primary')({ theme }), opacity)};
`
