import { css } from 'styled-components'

import media from './media'

export const regularFont = '"Lato", sans-serif'
export const headingFont = '"MontserratUENI", sans-serif'
export const regularFontFamily = `font-family: ${regularFont};`
export const headingFontFamily = `font-family: ${headingFont};`

// TODO: To confirm with @Tim on final design spacing and sizing

export const h1 = css`
  ${headingFontFamily}
  font-size: 2rem;
  font-weight: 800;
  letter-spacing: 0.04rem;
  line-height: 2.5rem;
  padding: 0.0625rem 0 0.875rem;

  ${media.tablet`
    letter-spacing: 0.01375rem;
  `};

  ${media.desktop`
    font-size: 3.5rem;
    font-weight: 800;
    letter-spacing: -0.26px;
    line-height: 4rem;
    padding: 0.75rem 0 0.25rem;
  `};
`

export const h2 = css`
  ${headingFontFamily}
  font-size: 1.5rem;
  font-weight: 800;
  letter-spacing: 0.04rem;
  line-height: 2rem;
  padding: 0 0.5rem;


  ${media.tablet`
    font-size: 2rem;
    letter-spacing: 0.01375rem;
    line-height: 3rem;
    padding: 0.813rem 0 0.25rem;
  `};

  ${media.desktop`
    font-size: 2.5rem;
    letter-spacing:  0.04rem;
    padding: 0.65rem 0.351rem;
  `};
`

export const h3 = css`
  ${headingFontFamily}
  font-size: 1.25rem;
  font-weight: 600;
  letter-spacing: 0.01375rem;
  line-height: 2rem;
  padding: 0.57rem 0.5rem;

  ${media.desktop`
    font-size: 2rem;
    line-height: 2.5rem;
    padding-top: 0.1rem;
  `};
`

export const h4 = css`
  ${headingFontFamily}
  font-size: 1.125rem;
  font-weight: 400;
  line-height: 1.5rem;
  letter-spacing: 0.01375rem;
  padding: 0.875rem 0 0.1rem;

  ${media.desktop`
    font-size: 1.5rem;
    padding: 0 0.75rem;
  `};
`

export const h5 = css`
  ${headingFontFamily}
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5rem;
  letter-spacing: 0.01375rem;
`
