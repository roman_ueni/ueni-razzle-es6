import { css } from 'styled-components'

export const large = css`
  font-size: 50px;
  height: 60px;
  width: 60px;
`

export const medium = css`
  font-size: 25px;
  height: 40px;
  width: 40px;
`

export const regular = css`
  font-size: 20px;
  height: 30px;
  width: 30px;
`

export const tight = css`
  font-size: 20px;
  height: 20px;
  width: 20px;
`

export const small = css`
  font-size: 12px;
  height: 20px;
  width: 20px;
`

export const smaller = css`
  font-size: 11px;
  height: 1rem;
  width: 1rem;
`
