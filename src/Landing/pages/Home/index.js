import React from 'react'
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'

import Hero from './Hero'

// eslint-disable-next-line
const LandingRoutes = ({ lang }) => (
  <div>
    <Hero/>
  </div>
)

export default connect(store => ({ lang: store.app.lang }))(LandingRoutes)
