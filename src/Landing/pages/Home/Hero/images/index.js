import barbers from './barbers.png'
import cakes from './cakes.png'
import carpentry from './carpentry.png'
import datepicker from './datepicker.png'
import example from './example.png'
import example2 from './example-2.png'
import gmb from './gmb.png'
import massage from './massage.png'
import services from './services.png'
import gList from './g-list.png'

export default [
  [carpentry],
  [example2, cakes],
  [example, datepicker, barbers],
  [gmb, massage, services],
  [gList, example],
]
