// @flow
import * as React from 'react'
import throttle from 'lodash/throttle'
import styled, { keyframes } from 'styled-components'
import { compose, withProps, withStateHandlers, withHandlers, lifecycle } from 'recompose'

import { Link } from 'react-scroll'

import { media, getColor } from '@shared/theme'
import { Flex } from '@shared/components'

import images from './images'

const Root = Flex.extend.attrs({ flexDirection: 'column' })`
  overflow: hidden;
`

const bounceAnimation = keyframes`
  0% {
    transform: translateY(-2px);
  }
  100% {
    transform: translateY(4px);
  }
`

const StepArrow = styled.div`
  display: table-cell;
  vertical-align: middle;
  position: static;
  width: 10%;
  animation: ${bounceAnimation} infinite 0.6s ease-in-out;
  animation-direction: alternate;
`

const Top = Flex.extend.attrs({
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
})`
  background-color: ${getColor('blue')};
  height: calc(100vh - 10rem);
  z-index: 1;
`

const Cards = Flex.extend.attrs({
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
})`
  transform: 
    translate(-30%, 5rem)
    scale(0.32)
    skew(21deg)
    rotateZ(-23deg)
    rotateY(25deg)
    rotateX(50deg);
  ${media.phoneLandscape`
    transform:
      translate(-20%, 6rem)
      scale(0.40)
      skew(21deg)
      rotateZ(-23deg)
      rotateY(25deg)
      rotateX(50deg);
  `};  
  ${media.tablet`
    transform:
      translate(-10%, 5rem)
      scale(0.50)
      skew(21deg)
      rotateZ(-23deg)
      rotateY(25deg)
      rotateX(50deg);
  `};
  ${media.desktop`
    transform: ${({ scale }) => `
      translate(8vw, 4.5rem)
      scale(${scale})
      skew(21deg)
      rotateZ(-23deg)
      rotateY(25deg)
      rotateX(50deg);
    `};
  `};
  ${media.largeDesktop`
    transform: ${({ scale, transformLeft }) => `
      translate(${transformLeft}px, 5.5rem)
      scale(${scale})
      skew(21deg)
      rotateZ(-23deg)
      rotateY(25deg)
      rotateX(50deg);
    `}
  `};
`

const Wrapper = styled.div`
  z-index: ${({ index }) => index};
  max-width: 391px;
`

const Text = styled.div`
  position: absolute;
  width: 100vw;
  top: 3rem;
  left: 1rem;
  max-width: 22rem;
  z-index: 0;
  ${media.phoneLandscape`
    left: 4rem;
  `};
  ${media.tablet`
    top: 6rem;
    max-width: 26rem;
  `};
  ${media.desktop`
    top: auto;
    left: 4rem;
    max-width: 32rem;
  `};
`

const Title = styled.h1`
  ${({ theme: { fonts: { h1 } } }) => h1};
  color: ${getColor('white')};
  margin-top: 0.5rem;
  ${media.tablet`
    font-size: 3rem;
    line-height: 3.5rem;
  `} ${media.desktop`
    font-size: 3.5rem;
    line-height: 4rem;
  `}
  padding: 0 !important;
`

const SubTitle = styled.h2`
  ${({ theme: { fonts: { h4 } } }) => h4};
  font-family: ${({ theme: { fonts: { regular } } }) => regular};
  color: ${getColor('secondary')};
  padding: 0 !important;
  ${media.phoneLandscape`
    font-size: 1.125rem;
  `};
`

const Image = styled.img`
  margin: 0.5rem;
  box-shadow: -42px 42px 34px 0 rgba(0, 0, 0, 0.5);
  border-radius: 1rem;
`

const Bottom = Flex.extend.attrs({
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
})`
  background-color: ${getColor('blueDarkest')};
  display: table;
  width:100%;
  height: 5rem;
  padding-left: 4rem;
`

const ShowMore = styled(Link)`
  ${({ theme: { fonts: { h4 } } }) => h4};
  font-size: 1.125rem !important;
  font-weight: 500;
  color: ${getColor('white')};
  text-align: left;
  letter-spacing: 0.3px;
  text-decoration: underline;
  visibility: hidden;
  display: table-cell;
  vertical-align: middle;
  ${media.phoneLandscape`
    visibility: visible;
    padding: 1rem 0;
  `};
`

type HeroPropsType = {
  scale: number,
  transformLeft: number,
}

const Hero = ({ scale, transformLeft }: HeroPropsType) => (
  <Root data-id="hero">
    <Top>
      <Text>
        <SubTitle>___T___homepageHeroSubTitle</SubTitle>
        <Title>___T___homepageHeroTitle</Title>
      </Text>
      <Cards scale={ scale } transformLeft={ transformLeft }>
        {images.map((chunk, index) => (
          <Wrapper key={ index } index={ images.length - index }>
            {chunk.map((image, key) => <Image key={ key } data-id={ key } src={ image }/>)}
          </Wrapper>
        ))}
      </Cards>
    </Top>
    <Bottom>
      <ShowMore to="features" data-id="show_more" spy smooth duration={ 500 }>
        {'___T___homepageFindOutMore'}
      </ShowMore>
      <StepArrow to="features" data-id="step_to_features"/>
    </Bottom>
  </Root>
)

const enhance = compose(
  withProps(() => ({
    cards: [
      ['carpentry'],
      ['example-2', 'cakes'],
      ['example', 'datepicker', 'barbers'],
      ['gmb', 'massage', 'services'],
      ['g-list', 'example'],
    ],
  })),
  withStateHandlers(
    { scale: 0, transformLeft: 0 },
    {
      onResize: () => () => {
        const { innerHeight, innerWidth } = window
        const min = Math.min(innerWidth, innerHeight)
        const scale = Math.min(min / 1000 - 0.3, 0.8)
        return {
          scale,
          transformLeft: window.innerWidth * 0.2,
        }
      },
    }
  ),
  withHandlers({
    onResizeThrottled: ({ onResize }) => throttle(onResize, 500),
  }),
  lifecycle({
    componentDidMount() {
      this.props.onResize()
      window.addEventListener('resize', this.props.onResizeThrottled, true)
    },
    componentWillUnmount() {
      window.removeEventListener('resize', this.props.onResizeThrottled, true)
    },
  })
)

export default enhance(Hero)
