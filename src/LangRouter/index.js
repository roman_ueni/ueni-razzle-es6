import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import config from '@config' // eslint-disable-line

import { setLanguage } from '../redux/app'

import DashboardRoutes from '../Dashboard'
import LandingRoutes from '../Landing'

@withRouter
@connect(store => ({ lang: store.app.lang }), {
  setDefaultLanguage: setLanguage,
})
export default class LangRouter extends React.PureComponent {

  componentWillMount() {
    // eslint-disable-next-line
    const { match: { params: { lang } }, location, history, setDefaultLanguage } = this.props

    /**
     * check if language is set (first param)
     *
     * first param can be mismatched for other routes
     * we need to check this
     */
    if (lang) {
      /**
       * language not supported
       * might be a direct route without param
       * supported languages are in src/config
       *
       */

      setDefaultLanguage(lang)

      if (!this.isLangSupported(lang)) {
        this.setRouteDefaultLang(config.defaultLang)
      }
    } else {
      this.setRouteDefaultLang(config.defaultLang)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { match: { params: { lang } }, setDefaultLanguage } = nextProps

    /**
     * save language change from route to redux store
     */
    // eslint-disable-next-line
    if (this.isLangSupported(lang) && lang !== this.props.match.params.lang) {
      setDefaultLanguage(lang)
    }
  }

  isLangSupported = lang => config.lang.indexOf(lang) !== -1

  setRouteDefaultLang = lang => {
    const { history, location } = this.props
    history.replace(`/${lang}${location.pathname}`)
  }

  render() {
    return (
      <Switch>
        <Route path="/:lang/hub" component={ DashboardRoutes }/>
        <Route component={ LandingRoutes }/>
      </Switch>
    )
  }

}
