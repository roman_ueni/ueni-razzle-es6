import React from 'react'

/**
 * this should be a router
 */

export default () => (
  <div
    style={ {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100vh',
    } }
  >
    <h2>Dashboard</h2>
  </div>
)
