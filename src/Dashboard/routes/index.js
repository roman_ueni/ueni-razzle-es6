import React from 'react'
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'

import Home from '../pages/Home'

// eslint-disable-next-line
const DashboardRoutes = ({ lang }) => (
  <Switch>
    <Route path={ `/${lang}/business` } component={ Home }/>
    <Redirect to={ `/${lang}/hub` }/>
  </Switch>
)

export default connect(store => ({ lang: store.app.lang }))(DashboardRoutes)
