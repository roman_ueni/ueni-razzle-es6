/* tslint:disable */

import { applyMiddleware, createStore } from 'redux'
import promiseMiddleware from 'redux-promise-middleware'

import initialState from './initialState'

import appReducer from './mainReducer'

export default () => {
  const isDev = process.env.NODE_ENV !== 'production'

  const middleware = [
    promiseMiddleware({
      promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
    }),
  ]

  if (isDev) {
    // Add Redux Logger
    const createLogger = require('redux-logger').createLogger // eslint-disable-line
    const logger = createLogger({
      collapsed: true,
    })
    middleware.push(logger)
  }

  const store = createStore(appReducer, initialState, applyMiddleware(...middleware))

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./', () => {
      store.replaceReducer(appReducer)
    })
  }

  return store
}
