const initialState = {
  lang: 'en-gb',
}

export const setLanguage = lang => ({
  payload: lang,
  type: '@app/LANGUAGE',
})

export default (state = initialState, action) => {
  switch (action.type) {

    case '@app/LANGUAGE':
      return {
        ...state,
        lang: action.payload,
      }

    default:
      return state
  
  }
}
