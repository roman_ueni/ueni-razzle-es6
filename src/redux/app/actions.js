export const setLanguage = lang => ({
  payload: lang,
  type: '@app/LANGUAGE',
})
