import { combineReducers } from 'redux'
import app from './app'

import initialState from './initialState'

const appReducer = combineReducers({
  app,
})

export default (state = initialState, action) => appReducer(state, action)
