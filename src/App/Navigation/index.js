import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

// eslint-disable-next-line
const Navigation = ({ lang }) => (
  <ul>
    <li>
      <Link to={ `/${lang}/hub` }>Go to hub</Link>
    </li>
    <li>
      <Link to={ `/${lang}` }>Go to Main</Link>
    </li>
    <li>
      <Link to="/somethui">Go to unknown lang</Link>
    </li>
    <li>
      <Link to={ `/${lang}/page` }>Go to unknown page</Link>
    </li>
  </ul>
)

export default connect(store => ({ lang: store.app.lang }))(Navigation)
