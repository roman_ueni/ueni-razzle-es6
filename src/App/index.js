import * as React from 'react'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { theme } from '@shared/theme'

import { createStore } from '@redux' // eslint-disable-line
import LangRouter from '../LangRouter'
import Navigation from './Navigation'

import './App.css'

const store = createStore()
console.log('THEME', theme)

const App = () => (
  <Provider store={ store }>
    <ThemeProvider theme={ theme }>
      <div>
        <Navigation/>
        <Switch>
          <Route path="/:lang" component={ LangRouter }/>
          <Route component={ LangRouter }/>
        </Switch>
      </div>
    </ThemeProvider>
  </Provider>
)

export default App
